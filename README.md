# Scripts

![Pipeline Status](https://gitlab.com/dgruzd/scripts/badges/main/pipeline.svg)

This repository contains a curated collection of productivity-enhancing scripts designed to streamline common tasks and workflows for developers and system administrators. From installing software and managing packages to interacting with files and automating repetitive processes, these scripts help save time, reduce manual effort, and enhance command-line efficiency. Many scripts feature interactive tools like `fzf` for a more intuitive user experience, and all can be accessed easily via a command-line interface using the `doer` script, which allows you to run them without needing to clone the entire repository.

## Table of Contents

<!-- TOC_START -->
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [How to Set Up](#how-to-set-up)
    - [Using Wrapper Script `doer`](#using-wrapper-script-doer)
    - [Cloning the Repository](#cloning-the-repository)
  - [Usage](#usage)
    - [Examples](#examples)
    - [Options](#options)
  - [Enabling Shell Completions](#enabling-shell-completions)
    - [Bash](#bash)
    - [Zsh](#zsh)
    - [Verifying Completion](#verifying-completion)
    - [Troubleshooting](#troubleshooting)
  - [Creating Aliases](#creating-aliases)
  - [How to Upgrade](#how-to-upgrade)
    - [If You Used the `doer` Script](#if-you-used-the-doer-script)
    - [If You Cloned the Repository](#if-you-cloned-the-repository)
  - [List of Scripts](#list-of-scripts)
  - [Security Considerations](#security-considerations)
  - [Demos](#demos)
<!-- TOC_END -->

## Introduction

The `doer` script acts as an interface for the scripts in this repository, allowing you to execute them conveniently without cloning the repository. This makes accessing and using these productivity scripts straightforward and efficient.

## How to Set Up

### Using Wrapper Script `doer`

*Note: `doer` currently stands for Developer-Oriented Execution Routine.*

1. Download the [`doer`](https://gitlab.com/dgruzd/scripts/-/blob/main/bin/doer) script to `~/.local/bin`:
   ```shell
   curl -f "https://gitlab.com/dgruzd/scripts/-/raw/main/bin/doer" -o ~/.local/bin/doer --create-dirs && \
     chmod 700 ~/.local/bin/doer
   ```
2. Ensure that `~/.local/bin` is part of your `$PATH`:
   ```shell
   # For Zsh
   echo 'export PATH=$HOME/.local/bin:$PATH' >> ~/.zshrc
   source ~/.zshrc

   # For Bash
   echo 'export PATH=$HOME/.local/bin:$PATH' >> ~/.bashrc
   source ~/.bashrc
   ```
3. Verify that it works:
   ```shell
   doer i
   ```

### Cloning the Repository

1. Add these lines to your rc file (e.g., `~/.zshrc` if you use Zsh) and clone the repository:
    ```shell
    # Please pick the directory that works for you
    echo 'export DGRUZD_SCRIPTS=~/projects/dgruzd-scripts' >> ~/.zshrc
    echo 'export PATH=$DGRUZD_SCRIPTS/bin:$PATH' >> ~/.zshrc
    source ~/.zshrc

    git clone git@gitlab.com:dgruzd/scripts.git $DGRUZD_SCRIPTS
    ```
2. Verify that it works:
   ```shell
   i
   ```

## Usage

The `doer` script allows you to execute any of the available scripts in the repository without needing to clone it. You can also clear the cache, list available scripts, or create aliases for frequently used scripts.

### Examples

- To list all available scripts:
  ```shell
  doer --list
  ```
- To execute a specific script, such as `i`
  ```shell
  doer i
  ```
- To clear the cache:
  ```shell
  doer --clear-cache
  ```
- For help:
  ```shell
  doer --help
  ```
- To create an alias for a frequently used script:
  ```shell
  doer --alias i
  ```
  This creates an alias script named `i` in the directory where `doer` is located.

  You can also specify a different alias name:
  ```shell
  doer --alias i my-brew
  ```
  This creates an alias script named `my-brew` that runs `doer i`.

### Options

- `-h`, `--help`: Display help message.
- `-l`, `--list`: List available scripts in the repository.
- `-c`, `--clear-cache`: Clear the locally cached scripts.
- `--completion [bash|zsh]`: Output shell completion code.
- `--alias [subcommand] [alias_name]`: Create an alias script for a subcommand (default `alias_name` is the same as `subcommand`).

## Enabling Shell Completions

To make using `doer` even more efficient, you can enable shell completion, which allows for the auto-completion of options and available script names. Follow the steps below to add completion support for your shell (Bash or Zsh).

### Bash

To enable Bash completion for `doer`, run the following command to generate the completion script:

```shell
source <(doer --completion bash)
```

To make this change permanent, add the command above to your shell profile (e.g., `~/.bashrc` or `~/.bash_profile`):

```shell
echo 'source <(doer --completion bash)' >> ~/.bashrc
```

Then, reload your profile:

```shell
source ~/.bashrc
```

### Zsh

To enable Zsh completion for `doer`, run the following command to generate the completion script:

```shell
source <(doer --completion zsh)
```

To make this change permanent, add the command above to your Zsh profile (e.g., `~/.zshrc`):

```shell
echo 'source <(doer --completion zsh)' >> ~/.zshrc
```

Then, reload your profile:

```shell
source ~/.zshrc
```

### Verifying Completion

After enabling the completion, you should be able to press `Tab` after typing `doer` to see available options and scripts. For example:

```shell
doer <Tab>
```

This should display a list of available commands, such as `--help`, `--list`, as well as any scripts that you have available in your repository.

**Note:** To enable subcommand completions, first run `doer --list` and then reload your terminal. This approach optimizes performance by preventing completions from affecting terminal startup time.

### Troubleshooting

- Make sure that `doer` is in your `PATH` and executable.
- If you encounter issues with completion, ensure that your shell supports programmable completion and that `bash-completion` or `zsh-completion` is installed.

## Creating Aliases

To simplify frequently used commands, you can create alias scripts using the `--alias` option. This will generate a new script in the same directory where `doer` is located, making it convenient to run a particular subcommand directly:

- **Default Alias Name**:
  ```shell
  doer --alias i
  ```
  This creates an alias script named `i`.

- **Custom Alias Name**:
  ```shell
  doer --alias i my-brew
  ```
  This creates an alias script named `my-brew`.

The alias scripts are executable and point to the `doer` script, effectively allowing you to run `i` or `my-brew` as standalone commands.

## How to Upgrade

To keep your scripts up to date, follow the appropriate steps based on your installation method.

### If You Used the `doer` Script

The `doer` script automatically checks for updates for itself and the other scripts in the repository every 7 days. When an update is available, you will be prompted to confirm the download. No manual steps are needed unless you wish to force an update.

To force an immediate update of `doer` and all scripts:
```shell
doer --clear-cache # Clears the cache, forcing fresh downloads on next use
```

### If You Cloned the Repository

1. Navigate to the project directory and pull the latest changes:
   ```shell
   cd $DGRUZD_SCRIPTS && git pull
   ```

This will download and apply the latest updates, ensuring that all your scripts are current.

## List of Scripts

<!-- TABLE_START -->
| Script    | Description |
| -------- | ------- |
| [`apt-install`](bin/apt-install) | This script has been deprecated. Please use the new script `i` instead. |
| [`apt-upgrade`](bin/apt-upgrade) | This script runs apt update && apt upgrade. |
| [`brew-install`](bin/brew-install) | This script has been deprecated. Please use the new script `i` instead. |
| [`brew-uninstall`](bin/brew-uninstall) | It is an fzf interface to `brew uninstall`. |
| [`fzf-files`](bin/fzf-files) | This script allows to interactively search and preview files within a directory using fzf and rg. |
| [`git-changed`](bin/git-changed) | This script outputs the list of files changed compared to the main branch. |
| [`hyperfine-zsh`](bin/hyperfine-zsh) | Script that uses hyperfine to compare zsh startup time with and without configuration. |
| [`i`](bin/i) | It is an fzf interface to install packages using either `brew` or `apt` depending on the OS. |
| [`jest-git`](bin/jest-git) | This script runs jest for all changed spec files in the current branch. |
| [`jest-watch`](bin/jest-watch) | This is a script to monitor changes in a git repository and execute jest for changed spec files only. |
| [`mr-status`](bin/mr-status) | This script fetches and displays open MRs assigned to you, with optional real-time monitoring and color-coded status based on open duration. |
| [`mr.rb`](bin/mr.rb) | It is a configurable wrapper for mr creation/updates that uses glab under the hood. For more info, please see [docs](/docs/mr.rb.md). |
| [`rspec-git`](bin/rspec-git) | This script runs rspec for all changed spec files in the current branch. |
| [`rspec-watch`](bin/rspec-watch) | This is a script to monitor changes in a git repository and execute rspec for changed spec files only. |
| [`watch-git`](bin/watch-git) | This script monitors changes in a git repository and executes a specified command. |
<!-- TABLE_END -->

## Security Considerations

The `doer` script fetches and executes other scripts from this repository, and it is essential to be transparent about how this process works to ensure user safety:

1. **Prompt Before Download/Update**: The `doer` script will always ask the user for confirmation before downloading or upgrading any script. You will be prompted with a `[Y/n]` option to proceed, ensuring that nothing is downloaded or executed without your explicit consent.

2. **Script Verification**: The `doer` script verifies that the downloaded script has a valid shebang (`#!`) line before executing it. This step ensures that only valid executable scripts are run.

3. **Cache Management**: Scripts are cached locally to avoid repeated downloads. The cache is automatically refreshed every 7 days to ensure you have the latest updates, but again, the update will require user consent.

4. **Code Visibility**: The source code for all scripts is publicly available, allowing you to inspect any script before executing it. This transparency helps ensure that there are no hidden or malicious commands.

## Demos

- [`brew-install`](https://asciinema.org/a/Jahd7JhKqOv1l4K1TMFL6tgwy)
- [`brew-uninstall`](https://asciinema.org/a/zOBMm7dOZ8cNAJ3retnD0tVxP)
