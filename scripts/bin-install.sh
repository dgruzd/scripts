#!/bin/bash

# LOGGING_BLOCK_START
# ANSI color codes (customizable)
RED='\033[0;31m'; GREEN='\033[0;32m'; YELLOW='\033[1;33m'; NC='\033[0m'
# Environment settings (defaults)
DEBUG_ENV_NAME="${DEBUG_ENV_NAME:-DEBUG}"; SCRIPT_NAME="${SCRIPT_NAME:-$(basename "$0")}"
# Main logging function
log_message() {
    local message="$1" level="${LEVEL:-INFO}" timestamp color="$NC"
    timestamp=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    [ -n "$COLOR" ] && case "$COLOR" in RED) color="$RED" ;; GREEN) color="$GREEN" ;; YELLOW) color="$YELLOW" ;; esac
    [ -z "$COLOR" ] && case "$level" in DEBUG) color="$YELLOW" ;; INFO) color="$NC" ;; ERROR) color="$RED" ;; esac
    local formatted_message="$timestamp [$level] - $SCRIPT_NAME - $message"
    [ -n "$STDOUT" ] && echo -e "${color}${formatted_message}${NC}" || echo -e "${color}${formatted_message}${NC}" >&2
}
# Wrapper functions for different log levels
debug() { [ -n "${!DEBUG_ENV_NAME}" ] && LEVEL="DEBUG" log_message "$1"; }
info() { LEVEL="INFO" log_message "$1"; }
error() { LEVEL="ERROR" log_message "$1"; }
# LOGGING_BLOCK_END

# Get the directory of the script
DIR="$( cd "$( dirname "$0" )" && pwd )"

# Get the repo root directory, which is one level above the script's directory
REPO_ROOT="$(dirname "$DIR")"

# Check if no arguments are provided, and set default value if so
if [ "$#" -eq 0 ]; then
  SCRIPT_PATH="bin/doer"
elif [ "$#" -eq 1 ]; then
  # If an argument is provided, consider it relative to the repo root
  SCRIPT_PATH="$1"
else
  error "Error: At most one argument is required."
  exit 1
fi

# Set the destination path
NEW_PATH="$HOME/.local/bin/$(basename "$SCRIPT_PATH")"

# Perform the copy operation silently and print our own custom message
if cp "$SCRIPT_PATH" "$NEW_PATH"; then
  info "$SCRIPT_PATH -> $NEW_PATH"
else
  error "Error: Failed to copy $SCRIPT_PATH to $NEW_PATH"
  exit 1
fi
