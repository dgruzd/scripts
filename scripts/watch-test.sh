#!/bin/bash
set -euo pipefail

DIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT_DIR="$DIR/.."
TEST_DIR="$ROOT_DIR/test"

ls $ROOT_DIR/bin/* $TEST_DIR/*.bats | entr -c "$ROOT_DIR/test/bats/bin/bats" -j $(nproc) $TEST_DIR
