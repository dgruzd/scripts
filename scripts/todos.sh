#!/bin/sh

# Use find to exclude the current script and .git directory, then grep for TODO
if find . -type f ! -name "$(basename "$0")" ! -path "./.git/*" ! -path "./test/*" -exec grep -Hn 'TODO' {} +; then
    echo "Please resolve all TODOs before merging."
    exit 1
fi
