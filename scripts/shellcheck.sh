#!/bin/sh
set -euo pipefail

# Check if shellcheck is installed
if ! command -v shellcheck &> /dev/null; then
    echo "Error: shellcheck is not installed."
    exit 1
fi

# Check if the script is run without arguments
if [ $# -eq 0 ]; then
    echo "Usage: $0 <arg1> <arg2> ..."
    exit 1
fi

# Use grep to filter out files with a shebang that contains 'sh'
shell_script_files=$(grep -lE '^#!.*sh' "$@" 2>/dev/null || true)

# Check if there are any shell scripts to pass to shellcheck
if [[ -z "$shell_script_files" ]]; then
    echo "No shell script files found to check."
    exit 0
fi

# Run shellcheck with the list of shell script files
# Using xargs to avoid issues with too many arguments
echo "$shell_script_files" | xargs shellcheck
