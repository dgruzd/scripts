#!/bin/bash
set -euo pipefail

DIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT_DIR="$DIR/.."
BIN_DIR="$ROOT_DIR/bin"

ls $BIN_DIR/* | entr -c "$DIR/shellcheck.sh" $BIN_DIR/*
