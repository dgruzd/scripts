#!/usr/bin/env python3

import os
import re
import sys

# Paths relative to the script's directory
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BIN_DIR = os.path.join(SCRIPT_DIR, '../bin')
README_FILE = os.path.join(SCRIPT_DIR, '../README.md')
LIST_FILE = os.path.join(SCRIPT_DIR, '../info/list.txt')

# Tags for the table in the README file
TABLE_START_TAG = '<!-- TABLE_START -->'
TABLE_END_TAG = '<!-- TABLE_END -->'

# Debug flag
DEBUG = '--debug' in sys.argv


def log_debug(message):
    """Log debug messages to stderr if debugging is enabled."""
    if DEBUG:
        print(f"DEBUG: {message}", file=sys.stderr)


def extract_header_description(script_path):
    """Extract the single-line description from the file header comment of the given script."""
    log_debug(f"Extracting description from {script_path}")
    with open(script_path, 'r') as script:
        for line in script:
            if line.lower().startswith("# description:"):
                # Extract and return the full description after `# description:`
                description = line.split(":", 1)[1].strip()
                log_debug(f"Found description: '{description}'")
                return description
    log_debug(f"No description found for {script_path}, using '-'")
    return "-"


def generate_table():
    """Generate the markdown table from the scripts in the ./bin directory."""
    rows = []

    log_debug(f"Generating table from scripts in {BIN_DIR}")
    for script in os.listdir(BIN_DIR):
        # Skip the entry if the script name is 'doer'
        if script == "doer":
            continue
        script_path = os.path.join(BIN_DIR, script)
        if os.path.isfile(script_path):
            log_debug(f"Processing script: {script}")
            description = extract_header_description(script_path)

            # Create relative path for the script link in the markdown table
            rel_script_path = os.path.relpath(script_path, start=os.path.dirname(README_FILE))
            rows.append((script, description, f"| [`{script}`]({rel_script_path}) | {description} |"))

    # Sort the rows by script name
    rows.sort(key=lambda x: x[0])
    log_debug(f"Sorted scripts: {[script for script, _, _ in rows]}")

    # Combine the rows into a markdown table
    if rows:
        table = "| Script    | Description |\n| -------- | ------- |\n" + "\n".join([row[2] for row in rows])
    else:
        table = "| Script    | Description |\n| -------- | ------- |\n"

    log_debug("Generated table:\n" + table)
    return rows, table

def readme_contains_table():
    """Read the README file and extract the existing table between TABLE_START_TAG and TABLE_END_TAG."""
    log_debug(f"Reading existing table from {README_FILE}")
    with open(README_FILE, 'r') as readme:
        content = readme.read()
        match = re.search(f"{TABLE_START_TAG}(.*?){TABLE_END_TAG}", content, re.DOTALL)
        if match:
            existing_table = match.group(1).strip()
            log_debug(f"Existing table found:\n{existing_table}")
            return existing_table
    log_debug("No existing table found")
    return None

def update_readme(new_table):
    """Update the README file with the new table, replacing the old table."""
    log_debug(f"Updating README file: {README_FILE}")
    with open(README_FILE, 'r') as readme:
        content = readme.read()

    updated_content = re.sub(
        f"{TABLE_START_TAG}.*?{TABLE_END_TAG}",
        f"{TABLE_START_TAG}\n{new_table}\n{TABLE_END_TAG}",
        content,
        flags=re.DOTALL
    )

    if updated_content != content:
        with open(README_FILE, 'w') as readme:
            readme.write(updated_content)
        log_debug("README.md updated.")
        print("README.md updated.")
    else:
        log_debug("README.md is up to date. No changes needed.")
        print("README.md is up to date. No changes needed.")

def update_list_file(rows):
    """Update the list.txt file with script names and descriptions, formatted with tabs for alignment."""
    log_debug(f"Updating list file: {LIST_FILE}")

    # Determine the maximum length of script names to calculate tab alignment
    max_script_length = max(len(script) for script, _, _ in rows)

    with open(LIST_FILE, 'w') as list_file:
        for script, description, _ in rows:
            # Skip the entry if the script name is 'doer'
            if script == "doer":
                continue

            # Replace backticks with single quotes in the description
            description = description.replace('`', "'")

            # Calculate the number of tabs needed to align the descriptions
            tab_count = (max_script_length - len(script)) // 8 + 1  # Assuming tab stop is every 8 characters
            tabs = "\t" * tab_count

            # Write the formatted line to the file
            list_file.write(f"{script}{tabs}{description}\n")

    log_debug(f"{LIST_FILE} updated with the list of scripts and descriptions.")

def main():
    # Check for --ci argument
    is_ci_mode = '--ci' in sys.argv

    # Generate the table and rows for the list file
    rows, new_table = generate_table()

    # If in CI mode, compare the tables and return appropriate code
    if is_ci_mode:
        existing_table = readme_contains_table()
        if existing_table and existing_table.strip() == new_table.strip():
            log_debug("Tables match in CI mode. Exiting with code 0.")
            sys.exit(0)
        else:
            log_debug("Tables do not match in CI mode. Exiting with code 1.")
            sys.exit(1)
    else:
        # Update the README file
        update_readme(new_table)
        # Update the list.txt file
        update_list_file(rows)


if __name__ == "__main__":
    main()
