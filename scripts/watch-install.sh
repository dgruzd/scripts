#!/bin/bash
DIR="$( cd "$( dirname "$0" )" && pwd )"

if [ "$#" -eq 0 ]; then
  SCRIPT_PATH="$DIR/../bin/doer"
elif [ "$#" -eq 1 ]; then
  SCRIPT_PATH="$1"
else
  echo "Error: At most one argument is required."
  exit 1
fi

ls "$SCRIPT_PATH" | entr -s "$DIR/bin-install.sh $SCRIPT_PATH"
