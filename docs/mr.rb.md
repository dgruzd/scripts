# `mr.rb`

Create or update merge requests. The script uses [`glab`](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/) under the hood.

## Create config

You might want to create a config file with `doer mr.rb --edit-config`:

```
glab_arguments:
  common:
    - --assignee=@me
  create:
    - --remove-source-branch
    - --squash-before-merge
labels:
  - group::global search
  - devops::data stores
  - section::core platform
reviewers:
  - username1
  - username2
```

This allows you to add labels and assign reviewers via `mr.rb`.

## Usage

```bash
doer mr.rb [flags]
```

## Examples

```bash
doer mr.rb -t "My amazing merge request" -d "My merge request description" -p

doer mr.rb --edit-config
doer mr.rb --dry-run
```

## Options

```plaintext
        --help                       Display this help
        --debug                      Enable debug mode
    -e, --edit-config                Edit config file
        --dry-run                    Perform dry run
    -p, --push                       Push committed changes
    -t, --title                      Set MR title
    -d, --description                Set MR description
    -m, --[no]-milestone             Set MR milestone from config file
```
