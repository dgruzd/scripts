#!/usr/bin/env bats

setup() {
  # This setup step will run before each test case.
  DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" && pwd )"
  ROOT_DIR="$DIR/.."
  DOER_SOURCE="$ROOT_DIR/bin/doer"

  DOER_TMP_DIR=$(mktemp -d)
  export DOER_CACHE_DIR="$DOER_TMP_DIR/cache"
  DOER_BIN_DIR="$DOER_TMP_DIR/bin"
  mkdir -p "$DOER_CACHE_DIR" "$DOER_BIN_DIR"
  cp "$DOER_SOURCE" "$DOER_BIN_DIR/"

  DOER="$DOER_BIN_DIR/doer"
}

teardown() {
  # Clean up the temporary cache directory after each test case
  rm -rf "$DOER_CACHE_DIR"
}

@test "doer runs without errors" {
  run $DOER
  [ "$status" -eq 0 ]
  [ "${#output}" -gt 0 ]  # Optionally check the output
}

@test "doer --help shows help message" {
  run $DOER --help
  [ "$status" -eq 0 ]
  [[ "$output" == *"Usage:"* ]]
  [[ "$output" == *"--help"* ]]
  [[ "$output" == *"--list"* ]]
  [[ "$output" == *"--clear-cache"* ]]
  [[ "$output" == *"--completion"* ]]
}

@test "doer --list retrieves list of scripts" {
  # Simulate a valid list.txt file
  echo "script1" > "$DOER_CACHE_DIR/list.txt"
  run $DOER --list
  [ "$status" -eq 0 ]
  [[ "$output" == *"script1"* ]]
}

@test "doer --clear-cache clears cache successfully" {
  # Create a dummy file in the cache
  touch "$DOER_CACHE_DIR/test_file"
  [ -f "$DOER_CACHE_DIR/test_file" ] # Confirm the file exists
  run $DOER --clear-cache
  [ "$status" -eq 0 ]
  [ ! -f "$DOER_CACHE_DIR/test_file" ] # Confirm the file is removed
}

@test "doer --completion bash generates bash completion script" {
  run $DOER --completion bash
  [ "$status" -eq 0 ]
  [[ "$output" == *"Bash completion for"* ]]
}

@test "doer --completion zsh generates zsh completion script" {
  run $DOER --completion zsh
  [ "$status" -eq 0 ]
  [[ "$output" == *"Zsh completion for"* ]]
}

@test "doer --alias creates an alias script" {
  # Simulate alias creation
  alias_name="my_alias"
  alias_script_path="$DOER_BIN_DIR/$alias_name"

  run $DOER --alias brew-install $alias_name
  [ "$status" -eq 0 ]
  [ -f "$alias_script_path" ]
  [[ "$(cat "$alias_script_path")" == *"exec"* ]]
}
