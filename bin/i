#!/bin/bash
# description: It is an fzf interface to install packages using either `brew` or `apt` depending on the OS.

set -euo pipefail

QUERY="${1:-}"

SCRIPT_NAME=$(basename "$0")

# [check_dependencies_start]
check_dependencies() {
    local missing=()

    # Text-based mapping for dependency URLs
    # Format: "command:url"
    local dependency_urls=(
        "fzf:https://github.com/junegunn/fzf#installation"
        "rg:https://github.com/BurntSushi/ripgrep#installation"
        "jq:https://github.com/jqlang/jq#installation"
        "git:https://git-scm.com/book/en/v2/Getting-Started-Installing-Git"
        "brew:https://brew.sh/"
        "apt:https://wiki.debian.org/Apt"
    )

    # Check each dependency passed to the function
    for cmd in "$@"; do
        if ! command -v "$cmd" >/dev/null 2>&1; then
            missing+=("$cmd")
        fi
    done

    # Handle the output for missing dependencies
    if [ ${#missing[@]} -ne 0 ]; then
        echo "The following dependencies are missing:"
        for cmd in "${missing[@]}"; do
            echo "  - $cmd"

            # Find the corresponding URL for the missing command
            local url_found="false"
            for entry in "${dependency_urls[@]}"; do
                local name="${entry%%:*}"
                local url="${entry#*:}"

                if [ "$name" = "$cmd" ]; then
                    echo "    Installation guide: $url"
                    url_found="true"
                    break
                fi
            done

            if [ "$url_found" = "false" ]; then
                echo "    No installation link provided. Please check your package manager."
            fi
        done
        echo "Please install the missing dependencies and try again."
        exit 1
    fi
}
# [check_dependencies_end]

# Function to use fzf for package selection
fzf_package_selection() {
    local preview_command="$1"

    # Read from the pipe
    fzf --multi \
        --query="$QUERY" \
        --prompt="Select packages to install> " \
        --preview="$preview_command" \
        --cycle \
        --height=40% \
        --min-height=32 \
        --layout=reverse
}

main() {
  # Determine the OS
  if [[ "$OSTYPE" == "darwin"* ]]; then
      # macOS
      OS="macOS"
      PACKAGE_MANAGER="brew"
      check_dependencies fzf brew
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
      # Linux
      OS="Linux"
      PACKAGE_MANAGER="apt"
      check_dependencies apt apt-cache fzf
  else
      echo "Unsupported operating system: $OSTYPE"
      exit 1
  fi

  # Cache settings for macOS using Homebrew
  if [ "$PACKAGE_MANAGER" = "brew" ]; then
      DATE=$(date -u "+%Y-%W")
      CACHE_DIR="$HOME/.cache/dgruzd_$SCRIPT_NAME"
      CACHE_FILE="$CACHE_DIR/$DATE.cache.gz"
      CACHE_GLOB="$CACHE_DIR/*.cache.gz"

      INFO_CACHE_DIR="$CACHE_DIR/info_cache"

      if [ ! -f "$CACHE_FILE" ]; then
          if ls $CACHE_GLOB >/dev/null 2>&1; then
              rm $CACHE_GLOB && rm -r $INFO_CACHE_DIR
          fi

          mkdir -p "$CACHE_DIR"
          if command -v brew formulae >/dev/null 2>&1; then
              {
                  brew formulae
                  brew casks
              } | gzip -c > "$CACHE_FILE"
          else
              # Fallback method using local tap repositories
              {
                  for tap in $(brew tap); do
                      repo=$(brew --repo "$tap")
                      find "$repo/Formula" -name '*.rb' -exec basename {} .rb \; 2>/dev/null
                      find "$repo/Casks" -name '*.rb' -exec basename {} .rb \; 2>/dev/null
                  done
              } | sort | uniq | gzip -c > "$CACHE_FILE"
          fi
      fi

      # Write the brew_info_cached function to a temporary script
      BREW_INFO_SCRIPT="$CACHE_DIR/brew_info_cached.sh"
      cat > "$BREW_INFO_SCRIPT" <<'EOF'
#!/bin/bash
set -euo pipefail

INFO_CACHE_DIR="$1"
formula="$2"
mkdir -p "$INFO_CACHE_DIR"
safe_formula=$(echo "$formula" | tr '/\\:*?"<>|' '_')
cache_file="$INFO_CACHE_DIR/${safe_formula}.info"

if [ -f "$cache_file" ]; then
    # Check if the file is less than 1 hour old (3600 seconds)
    if [ $(($(date +%s) - $(stat -f %m "$cache_file"))) -lt 3600 ]; then
        cat "$cache_file"
        exit
    fi
fi

brew info "$formula" 2>/dev/null | tee "$cache_file"
EOF

      chmod +x "$BREW_INFO_SCRIPT"
      export HOMEBREW_COLOR=1

      # Use the `fzf_package_selection` function for brew
      inst=$(gzip -dc "$CACHE_FILE" | fzf_package_selection "$BREW_INFO_SCRIPT '$INFO_CACHE_DIR' {}")

      if [[ $inst ]]; then
          brew install $inst
      fi

  # Cache settings for Linux using apt
  elif [ "$PACKAGE_MANAGER" = "apt" ]; then
      # Generate package list and use the `fzf_package_selection` function for apt
      inst=$(apt-cache search . | awk '{print $1}' | fzf_package_selection "apt show {} 2> /dev/null")

      # Check if any packages were selected
      if [ -n "$inst" ]; then
          # Install the selected packages
          sudo apt install -y -qq $inst
      else
          echo "No packages selected."
      fi
  fi
}

main "$@"; exit
